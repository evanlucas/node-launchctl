# node-launchctl

[![Build Status](https://travis-ci.org/evanlucas/node-launchctl.png?branch=master)](https://travis-ci.org/evanlucas/node-launchctl)

Provides native bindings to launchctl commands

[![NPM](https://nodei.co/npm/launchctl.png?downloads=true)](https://nodei.co/npm/launchctl/)

## It has been tested with the following version:

- 0.8.26
- 0.10.x
- 0.11.4, 0.11.5, 0.11.6, 0.11.7, 0.11.8, 0.11.9, and 0.11.10

## Dependencies

- Tested on OS X 10.7.5+
- Requires Xcode 4.5+

## Install

```bash
$ npm install launchctl
```

## Usage

```js
var ctl = require('launchctl')
```

## Test

```bash
$ npm test
```

- To run tests for multiple versions

```bash
$ npm run test-versions
```

## API

 [Documentation](http://evanlucas.github.io/node-launchctl)

## TODO

- Make API more complete

## Contributions

- Please feel free to fork/contribute :]

